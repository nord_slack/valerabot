package com.valera.valerabot.controlers

import com.valera.valerabot.services.FarcicalService
import com.valera.valerabot.services.TelegramApi
import com.valera.valerabot.services.handlers.WeatherHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
class SimpleController {

    @Autowired
    private lateinit var telegramApi: TelegramApi

    @Autowired
    private lateinit var farcicalService: FarcicalService

    @Autowired
    private lateinit var weatherHandler: WeatherHandler

    @GetMapping("status")
    fun getStatus(): String {
        return telegramApi.getStatus()
    }

    @GetMapping("test/{name}")
    fun getTest(@PathVariable name: String): String {
        return weatherHandler.handleMessage(1L, "Бот яка погода у Vinnytsia завтра?")
    }

    @PostMapping("update")
    fun update(@RequestBody update: Update) {
        if (update.channel_post.message_id > 0) {
            farcicalService.process(update.channel_post.chat.id, update.channel_post.text)
        } else if (update.message.message_id > 0) {
            farcicalService.process(update.message.chat.id, update.message.text)
        }
    }
}