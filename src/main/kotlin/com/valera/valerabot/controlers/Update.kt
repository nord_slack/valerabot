package com.valera.valerabot.controlers

import java.util.*


data class Update (
    val update_id: Int = 0,
    var message: Message = Message(),
    var inline_query: String = "",
    var chosen_inline_result: String = "",
    var callback_query: String = "",
    var channel_post: ChannelPost = ChannelPost()
)

data class Entity(
    var offset: Int = 0,
    var length: Int = 0,
    var type: String = ""
)

data class Message(
    var message_id: Int = 0,
    var from: From = From(),
    var chat: Chat = Chat(),
    var date: Long = 0,
    var text: String = "",
    var entities: List<Entity> = ArrayList()
)

data class Chat(
    var id: Long = 0,
    var title: String = "",
    var username: String = "",
    var type: String = "",
    var all_members_are_administrators: Boolean = false
)

data class From(
    var id: Long = 0,
    var is_bot: Boolean = false,
    var first_name: String = "",
    var last_name: String = "",
    var language_code: String = ""
)

data class ChannelPost(
    var message_id: Int = 0,
    var chat: Chat = Chat(),
    var date: Long = 0,
    var text: String = "",
    var entities: List<Entity> = ArrayList()
)