package com.valera.valerabot

import com.valera.valerabot.services.FarcicalService
import com.valera.valerabot.services.handlers.DefaultMessageHandler
import com.valera.valerabot.services.handlers.SmallTalkMessageHandler
import com.valera.valerabot.services.handlers.TracktoristMessageHandler
import com.valera.valerabot.services.handlers.WeatherHandler
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import java.time.Clock



@SpringBootApplication
class ValeraBotApplication{
	@Bean
	fun smallTalkMessageHandler(): SmallTalkMessageHandler {
		return SmallTalkMessageHandler()
	}

	@Bean
	fun defaultMessageHandler(): DefaultMessageHandler {
		return DefaultMessageHandler(smallTalkMessageHandler())
	}

	@Bean
	fun tracktoristMessageHandler(): TracktoristMessageHandler {
		return TracktoristMessageHandler(defaultMessageHandler())
	}

	@Bean
	fun whetherHandler(): WeatherHandler {
		return WeatherHandler(tracktoristMessageHandler())
	}

	@Bean
	fun farcicalService(): FarcicalService {
		return FarcicalService(whetherHandler())
	}

	@Bean
	fun clock(): Clock {
		return Clock.systemDefaultZone()
	}
}

fun main(args: Array<String>) {
	runApplication<ValeraBotApplication>(*args)
}
