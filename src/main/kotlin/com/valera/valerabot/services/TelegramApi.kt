package com.valera.valerabot.services

import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import java.net.URL
import java.net.URLEncoder

@Component
class TelegramApi {

    fun getStatus(): String {
        return URL("https://api.telegram.org/bot840177909:AAGIQzGExlF5GzEoDbmiqCYRzjCvUwhsOy4/getMe").readText()
    }

    fun sendMessage(channelId:Long, text:String) {
        URL("https://api.telegram.org/bot840177909:AAGIQzGExlF5GzEoDbmiqCYRzjCvUwhsOy4/sendMessage?chat_id=$channelId&text=" + URLEncoder.encode(text, "UTF-8")).readText()
    }
}