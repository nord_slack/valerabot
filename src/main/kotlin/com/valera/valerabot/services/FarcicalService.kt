package com.valera.valerabot.services

import com.valera.valerabot.services.handlers.*
import org.springframework.beans.factory.annotation.Autowired
import java.nio.charset.Charset

class FarcicalService(val chain: MessageHandler) {

    @Autowired
    private lateinit var telegramApi: TelegramApi

    fun process(channelId:Long, text:String){
        val message = chain.handleMessage(channelId, text)
        if (message.isNotEmpty()) {
            telegramApi.sendMessage(channelId, message)
        }
    }
}