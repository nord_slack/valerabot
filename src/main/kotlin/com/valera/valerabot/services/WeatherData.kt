package com.valera.valerabot.services

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class WeatherData(
        val city: City = City(),
        val list: List<Info> = ArrayList()
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Info(
        val temp: Temp = Temp(),
        val weather: List<Weather> = ArrayList()
)
@JsonIgnoreProperties(ignoreUnknown = true)
data class Temp(
        val day: Double = 0.0,
        val night: Double = 0.0
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class City(
        val id: Int = 0,
        val name: String = ""
)
