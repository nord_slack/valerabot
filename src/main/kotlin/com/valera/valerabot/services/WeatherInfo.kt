package com.valera.valerabot.services

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class WeatherInfo(
        val name: String = "",
        val id: Int = 0,
        val main: Main = Main(),
        val weather: ArrayList<Weather> = ArrayList()

)


@JsonIgnoreProperties(ignoreUnknown = true)
data class Main(
        val temp: Double = 0.0
)

