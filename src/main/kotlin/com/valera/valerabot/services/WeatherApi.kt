package com.valera.valerabot.services

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import java.net.URL

@Component
class WeatherApi {
    fun getWeatherByCityName(name: String): WeatherData {
        val openConnection = URL("https://community-open-weather-map.p.rapidapi.com/forecast/daily?cnt=10&units=metric&mode=json&lang=ua&q=$name").openConnection()
        openConnection.setRequestProperty("x-rapidapi-host", "community-open-weather-map.p.rapidapi.com")
        openConnection.setRequestProperty("x-rapidapi-key", "0ab30998efmsha84457d021cd58ap10b4b6jsnc2996554f41d")
        val responseJson = openConnection.getInputStream().reader().readText()
        val objectMapper = ObjectMapper()
        return objectMapper.readValue<WeatherData>(responseJson, WeatherData::class.java)
    }

    fun getCurrentWeatherByCityName(name: String): WeatherInfo {
        val openConnection = URL("https://community-open-weather-map.p.rapidapi.com/weather?units=metric&mode=json&lang=ua&q=$name").openConnection()
        openConnection.setRequestProperty("x-rapidapi-host", "community-open-weather-map.p.rapidapi.com")
        openConnection.setRequestProperty("x-rapidapi-key", "0ab30998efmsha84457d021cd58ap10b4b6jsnc2996554f41d")
        val responseJson = openConnection.getInputStream().reader().readText()
        val objectMapper = ObjectMapper()
        return objectMapper.readValue<WeatherInfo>(responseJson, WeatherInfo::class.java)
    }

}
