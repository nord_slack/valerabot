package com.valera.valerabot.services.handlers

import com.valera.valerabot.services.WeatherApi
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import java.time.Clock
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.math.roundToInt


class WeatherHandler(val chain: MessageHandler) : MessageHandler {
    private val DEFAULT_WHETHER_MESSAGE = "Погода заїбісь"

    @Autowired
    lateinit var weatherApi: WeatherApi

    @Autowired
    lateinit var clock: Clock

    private val logger: Logger = LoggerFactory.getLogger(WeatherHandler::class.java)

    override fun handleMessage(chatId: Long, message: String): String {
        if ((message.contains("уточни", true) || message.contains("як", true) || message.contains("яка", true) || message.contains("какая", true))
                && (message.contains("погода", true) || message.contains("погодка", true) || message.contains("погодку", true))) {
            try {
                val cityName: String
                logger.info(message)
                val wordList = message.replace("[.,?!]".toRegex(), "").split(" ")
                var index = wordList.indexOf("у")
                if (index < 0) {
                    index = wordList.indexOf("в")
                }
                if (index < 0) {
                    cityName = "Vinnytsia"
                } else {
                    cityName = wordList.get(index + 1)
                }
                val name: String
                val weatherDescription: String
                val temp: String
                val dateIndex = retrieveDate(message)
                if (dateIndex == 0) {
                    val weatherInfo = weatherApi.getCurrentWeatherByCityName(cityName)
                    name = weatherInfo.name
                    weatherDescription = weatherInfo.weather[0].description
                    temp = "температура %.0f".format(weatherInfo.main.temp.roundToInt())
                } else {
                    val weatherData = weatherApi.getWeatherByCityName(cityName)
                    name = weatherData.city.name
                    val item = weatherData.list[dateIndex]
                    weatherDescription = item.weather[0].description
                    temp = "температура у день ${item.temp.day.roundToInt()} та у вечорі ${item.temp.night.roundToInt()}"
                }
                return "Погода у місті ${name} ${weatherDescription}, $temp"
            } catch (e: Exception) {
                return DEFAULT_WHETHER_MESSAGE
            }
        }

        return chain.handleMessage(chatId, message)
    }

    private fun retrieveDate(message: String): Int {
        val wordList = message.toLowerCase().replace("[.,?!]".toRegex(), "").split(" ")
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")

        if (wordList.indexOf("завтра") > -1) return 1
        else if (wordList.indexOf("післязавтра") > -1 || wordList.indexOf("послезавтра") > -1) return 2
        for (word in wordList) {
            try {
                val date = LocalDate.parse(word, formatter)
                val now = LocalDate.now(clock)
                return (date.toEpochDay() - now.toEpochDay()).toInt();
            } catch (e: Exception) {

            }
        }
        return 0
    }
}