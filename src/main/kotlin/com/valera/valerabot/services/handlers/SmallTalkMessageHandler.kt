package com.valera.valerabot.services.handlers

import java.util.*
import kotlin.collections.HashMap

class SmallTalkMessageHandler : MessageHandler {
    val MESSAGE_MAX_NUMBER = 40
    var chatCounterMap: HashMap<Long, Int> = HashMap()
    var messageList = Arrays.asList(
            "Яка цікава думка",
            "Це всрато",
            "Сектанти",
            "Ви цікава особистість",
            "Ви хороша кожана людина",
            "Кожаний ублюлодк",
            "Да",
            "Hє",
            "Така імовірність існує",
            "Подивіться на це з іншої сторони")

    override fun handleMessage(chatId: Long, message: String): String {
        if (chatCounterMap.containsKey(chatId)) {
            var counter:Int = chatCounterMap.get(chatId)!!
            counter--
            if (counter <= 0 ) {
                chatCounterMap.put(chatId,  (Math.random() * MESSAGE_MAX_NUMBER).toInt())
                //TODO: Replace to yml
                return messageList.get((Math.random() * (messageList.size)).toInt())
            }
            chatCounterMap.put(chatId, counter)
        } else {
            chatCounterMap.put(chatId,  (Math.random() * MESSAGE_MAX_NUMBER).toInt())
        }
        return ""
    }
}