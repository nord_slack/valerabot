package com.valera.valerabot.services.handlers

class TracktoristMessageHandler(val chain: MessageHandler) : MessageHandler {
    val TRACKTORIST_JOCKE_MESSAGE = "Отсосі у тракториста"

    override fun handleMessage(chatId: Long, message: String): String {
        if(     message.endsWith("300") ||
                message.endsWith("риста") ||
                message.endsWith("ріста") ||
                message.endsWith("рыста")) {
            return TRACKTORIST_JOCKE_MESSAGE
        }
        return chain.handleMessage(chatId, message)
    }
}