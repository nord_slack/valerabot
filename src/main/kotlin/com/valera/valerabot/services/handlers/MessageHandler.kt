package com.valera.valerabot.services.handlers

interface MessageHandler {

    fun handleMessage(chatId: Long, message: String): String
}