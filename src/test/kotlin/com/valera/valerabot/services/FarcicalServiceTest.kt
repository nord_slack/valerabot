package com.valera.valerabot.services

import org.junit.Ignore
import org.junit.runner.RunWith

import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

//TODO: move to functial test
@Ignore
@RunWith(SpringJUnit4ClassRunner::class)
class FarcicalServiceTest {
    val CHANNEL_ID: Long = 123
    val DEFAULT_MESSAGE = "Іди на хуй"
    val TRACKTORIST_JOCKE_MESSAGE = "Отсосі у тракториста"
    val DEFAULT_WHETHER_MESSAGE = "Погода заїбісь"

    @Mock
    lateinit var telegramApi:TelegramApi

    @InjectMocks
    lateinit var farcicalService: FarcicalService

    @Test
    fun testSendDefaultMessageIfTextHasBotNameLink() {
        farcicalService.process(CHANNEL_ID, "ololo mass @valerius_domesticus_vulgaris_bot another mass")

        verify(telegramApi).sendMessage(CHANNEL_ID, DEFAULT_MESSAGE)
    }

    @Test
    fun testDoNotSendDefaultMessageIfTextHasNotBotNameLink() {
        farcicalService.process(CHANNEL_ID, "ololo mass")

        verify(telegramApi, never()).sendMessage(anyLong(), anyString())
    }

    @Test
    fun testSendTractoristJokeIfTextEndsWith300() {
        farcicalService.process(CHANNEL_ID, " ololo mass 300   ")
        farcicalService.process(CHANNEL_ID, " ololo mass риста   ")
        farcicalService.process(CHANNEL_ID, " ololo mass ріста   ")
        farcicalService.process(CHANNEL_ID, " ololo mass рыста   ")

        verify(telegramApi, times(4)).sendMessage(CHANNEL_ID, TRACKTORIST_JOCKE_MESSAGE)
    }

    @Test
    fun testSendDefaultWhetherIfTextHasCommand() {
        farcicalService.process(CHANNEL_ID, "/ some message")

        verify(telegramApi).sendMessage(CHANNEL_ID, DEFAULT_WHETHER_MESSAGE)
    }
}
