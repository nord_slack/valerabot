package com.valera.valerabot.services.handlers

import com.valera.valerabot.services.*
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.time.LocalDate
import java.time.Clock
import org.mockito.Mockito.`when`
import java.time.ZoneId


@RunWith(SpringJUnit4ClassRunner::class)
class WeatherHandlerTest {
    val DEFAULT_WHETHER_MESSAGE = "Погода заїбісь"
    val CHAT_ID: Long = 0
    val CITY_NAME = "Vinnytsia"
    val CURRENT_LOCAL_DATE = LocalDate.of(2019, 5, 4)

    @Mock
    lateinit var chain: MessageHandler

    @Mock
    lateinit var weatherApi: WeatherApi

    @Mock
    lateinit var clock: Clock

    @InjectMocks
    lateinit var weatherHandler: WeatherHandler

    lateinit var fixedClock: Clock

    @Before
    fun setUp() {
        weatherHandler.weatherApi = weatherApi

        fixedClock = Clock.fixed(CURRENT_LOCAL_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
        `when`((clock).instant()).thenReturn(fixedClock.instant())
        `when`((clock).getZone()).thenReturn(fixedClock.zone)
        weatherHandler.clock = clock
    }

    @Test
    fun getDefaultMessageForCurrentWhether() {
        weatherHandler.handleMessage(CHAT_ID, "яка погода ?")

        verify(weatherApi).getCurrentWeatherByCityName(CITY_NAME)
    }

    @Test
    fun callChainHandlerForNonCommand() {
        val message = "some message погода"
        weatherHandler.handleMessage(CHAT_ID, message)

        verify(chain).handleMessage(CHAT_ID, message)
    }

    @Test
    fun callActualWhetherByTheCityName() {
        weatherHandler.handleMessage(CHAT_ID, "Яка погода у $CITY_NAME?")

        verify(weatherApi).getCurrentWeatherByCityName(CITY_NAME)
    }

    @Test
    fun callTomorrowWhether() {
        var list = ArrayList<Info>()
        var weather = ArrayList<Weather>()
        weather.add(Weather("ololo"))
        list.add(Info(
                Temp(10.0, 10.0),
                weather
        ))
        list.add(Info(
                Temp(20.0, 20.0),
                weather
        ))
        list.add(Info(
                Temp(30.0, 30.0),
                weather
        ))
        list.add(Info(
                Temp(40.0, 40.0),
                weather
        ))
        list.add(Info(
                Temp(50.0, 50.0),
                weather
        ))
        list.add(Info(
                Temp(60.0, 60.0),
                weather
        ))
        list.add(Info(
                Temp(70.0, 70.0),
                weather
        ))
        list.add(Info(
                Temp(80.0, 80.0),
                weather
        ))
        list.add(Info(
                Temp(90.0, 90.0),
                weather
        ))
        list.add(Info(
                Temp(100.0, 100.0),
                weather
        ))

        val weatherData = WeatherData(City(0, CITY_NAME), list)
        Mockito.`when`(weatherApi.getWeatherByCityName(CITY_NAME)).thenReturn(weatherData)

        val actualMessage = weatherHandler.handleMessage(CHAT_ID, "Бот яка погода у $CITY_NAME завтра?")

        verify(weatherApi).getWeatherByCityName(CITY_NAME)
        assertEquals("Погода у місті $CITY_NAME ololo, температура у день 20 та у вечорі 20", actualMessage)
    }

    @Test
    fun callNextTomorrowWhether() {
        var list = ArrayList<Info>()
        var weather = ArrayList<Weather>()
        weather.add(Weather("ololo"))
        list.add(Info(
                Temp(10.0, 10.0),
                weather
        ))
        list.add(Info(
                Temp(20.0, 20.0),
                weather
        ))
        list.add(Info(
                Temp(30.0, 30.0),
                weather
        ))
        list.add(Info(
                Temp(40.0, 40.0),
                weather
        ))
        list.add(Info(
                Temp(50.0, 50.0),
                weather
        ))
        list.add(Info(
                Temp(60.0, 60.0),
                weather
        ))
        list.add(Info(
                Temp(70.0, 70.0),
                weather
        ))
        list.add(Info(
                Temp(80.0, 80.0),
                weather
        ))
        list.add(Info(
                Temp(90.0, 90.0),
                weather
        ))
        list.add(Info(
                Temp(100.0, 100.0),
                weather
        ))

        val weatherData = WeatherData(City(0, CITY_NAME), list)
        Mockito.`when`(weatherApi.getWeatherByCityName(CITY_NAME)).thenReturn(weatherData)

        val actualMessage = weatherHandler.handleMessage(CHAT_ID, "Яка погода у $CITY_NAME Післязавтра?")

        verify(weatherApi).getWeatherByCityName(CITY_NAME)
        assertEquals("Погода у місті $CITY_NAME ololo, температура у день 30 та у вечорі 30", actualMessage)
    }

    @Test
    fun callByDateWhether() {
        var list = ArrayList<Info>()
        var weather = ArrayList<Weather>()
        weather.add(Weather("ololo"))
        list.add(Info(
                Temp(10.0, 10.0),
                weather
        ))
        list.add(Info(
                Temp(20.0, 20.0),
                weather
        ))
        list.add(Info(
                Temp(30.0, 30.0),
                weather
        ))
        list.add(Info(
                Temp(40.0, 40.0),
                weather
        ))
        list.add(Info(
                Temp(50.0, 50.0),
                weather
        ))
        list.add(Info(
                Temp(60.0, 60.0),
                weather
        ))
        list.add(Info(
                Temp(70.0, 70.0),
                weather
        ))
        list.add(Info(
                Temp(80.0, 80.0),
                weather
        ))
        list.add(Info(
                Temp(90.0, 90.0),
                weather
        ))
        list.add(Info(
                Temp(100.0, 100.0),
                weather
        ))

        val weatherData = WeatherData(City(0, CITY_NAME), list)
        `when`(weatherApi.getWeatherByCityName(CITY_NAME)).thenReturn(weatherData)

        val actualMessage = weatherHandler.handleMessage(CHAT_ID, "Яка погода у $CITY_NAME 13-05-2019?")

        verify(weatherApi).getWeatherByCityName(CITY_NAME)
        assertEquals("Погода у місті $CITY_NAME ololo, температура у день 100 та у вечорі 100", actualMessage)
    }


}
