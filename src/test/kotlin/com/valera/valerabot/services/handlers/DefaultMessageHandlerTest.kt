package com.valera.valerabot.services.handlers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(SpringJUnit4ClassRunner::class)
class DefaultMessageHandlerTest{
    val CHAT_ID:Long = 1
    val DEFAULT_MESSAGE = "Іди на хуй"

    @Mock
    lateinit var chain: MessageHandler

    @InjectMocks
    lateinit var defaultMessageHandler: DefaultMessageHandler

    @Test
    fun testGetDefaultMessageIfTextHasBotNameLink() {
        val actual = defaultMessageHandler.handleMessage(CHAT_ID,"ololo mass @valerius_domesticus_vulgaris_bot another mass")

        assertEquals(DEFAULT_MESSAGE, actual)
    }

    @Test
    fun testCallChainHandlerIfTextDoesNotHasBotNameLink() {
        val message = "ololo mass "
        defaultMessageHandler.handleMessage(CHAT_ID, message)

        verify(chain).handleMessage(CHAT_ID, message)
    }
}