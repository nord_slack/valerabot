package com.valera.valerabot.services.handlers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(SpringJUnit4ClassRunner::class)
class SmallTalkMessageHandlerTest {
    @InjectMocks
    lateinit var smallTalkMessageHandler: SmallTalkMessageHandler

    @Test
    fun testGetSmallTalkMessageIfChatCounterIsEnough() {
        smallTalkMessageHandler.handleMessage(1, "dfwsfwf")
        smallTalkMessageHandler.handleMessage(1, "dfwsfwf")
        smallTalkMessageHandler.handleMessage(1, "dfwsfwf")
        smallTalkMessageHandler.handleMessage(1, "dfwsfwf")
    }
}