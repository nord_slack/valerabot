package com.valera.valerabot.services.handlers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@RunWith(SpringJUnit4ClassRunner::class)
class TracktoristMessageHandlerTest {
    val CHAT_ID:Long = 1
    val TRACKTORIST_JOCKE_MESSAGE = "Отсосі у тракториста"
    @Mock
    lateinit var messageHandler: MessageHandler

    @InjectMocks
    lateinit var tracktoristMessageHandler: TracktoristMessageHandler

    @Test
    fun testCallChainHandlerIfMessageWithout300Ending() {
        val message = "ololo mass @valerius_domesticus_vulgaris_bot another mass"
        tracktoristMessageHandler.handleMessage(CHAT_ID, message)

        verify(messageHandler).handleMessage(CHAT_ID, message)
    }

    @Test
    fun testGetTracktoristTextIfMessagehas300Ending() {
        var actual = tracktoristMessageHandler.handleMessage(CHAT_ID," ololo mass 300")
        assertEquals(TRACKTORIST_JOCKE_MESSAGE, actual)
        actual = tracktoristMessageHandler.handleMessage(CHAT_ID," ololo mass риста")
        assertEquals(TRACKTORIST_JOCKE_MESSAGE, actual)
        actual = tracktoristMessageHandler.handleMessage(CHAT_ID," ololo mass ріста")
        assertEquals(TRACKTORIST_JOCKE_MESSAGE, actual)
        actual = tracktoristMessageHandler.handleMessage(CHAT_ID," ololo mass рыста")
        assertEquals(TRACKTORIST_JOCKE_MESSAGE, actual)

        verify(messageHandler, never()).handleMessage(ArgumentMatchers.anyLong(), anyString())
    }
}