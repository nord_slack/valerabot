package com.valera.valerabot.controlers

import com.valera.valerabot.services.FarcicalService
import com.valera.valerabot.services.TelegramApi
import junit.framework.TestCase.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.mockito.Mockito.verify


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
internal class SimpleControllerTest {
    @Autowired
    lateinit var testRestTemplate:TestRestTemplate

    @MockBean
    lateinit var telegramApi: TelegramApi

    @MockBean
    lateinit var farcicalService: FarcicalService

    @Test
    fun testCallStatus() {
        val result = testRestTemplate.getForEntity("/status", String::class.java)
        assertNotNull(result)
        assertTrue(true)
    }

    @Test
    fun testCallFarcicalServiceIfChannelPostIdNotZero() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val channelId = -1001328555360
        val text = "@valerius_domesticus_vulgaris_bot ololo"
        val request:HttpEntity<String> = HttpEntity("{\"update_id\":658103169,\n\"channel_post\":{\"message_id\":23,\"chat\":{\"id\":$channelId,\"title\":\"Tttost\",\"username\":\"tttost\",\"type\":\"channel\"},\"date\":1559907643,\"text\":\"$text\",\"entities\":[{\"offset\":0,\"length\":33,\"type\":\"mention\"}]}}", headers)

        val result = testRestTemplate.postForEntity("/update", request, String::class.java)

        assertEquals(HttpStatus.OK, result.statusCode)
        verify(farcicalService).process(channelId, text)
    }

    @Test
    fun testCallFarcicalServiceIfMessageIdNotZero() {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val channelId = -364418337L
        val text = "@valerius_domesticus_vulgaris_bot ololo"
        val request:HttpEntity<String> = HttpEntity("{\"update_id\":658103290,\n\"message\":{\"message_id\":128,\"from\":{\"id\":166694634,\"is_bot\":false,\"first_name\":\"Carl\",\"last_name\":\"Slow\",\"language_code\":\"uk\"},\"chat\":{\"id\":-364418337,\"title\":\"\\u0414\\u043f\\u0441\\u0447\",\"type\":\"group\",\"all_members_are_administrators\":true},\"date\":1561495939,\"text\":\"$text\",\"entities\":[{\"offset\":0,\"length\":33,\"type\":\"mention\"}]}}", headers)

        val result = testRestTemplate.postForEntity("/update", request, String::class.java)

        assertEquals(HttpStatus.OK, result.statusCode)
        verify(farcicalService).process(channelId, text)
    }
}
